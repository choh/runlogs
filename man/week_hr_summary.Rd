% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/week_hr_summary.R
\name{week_hr_summary}
\alias{week_hr_summary}
\title{Generate summary for heart rate for a week.}
\usage{
week_hr_summary(db, week = NULL, year = NULL)
}
\arguments{
\item{db}{The database file to query.}

\item{week}{The week to retrieve data for.}

\item{year}{The year to retrieve data for.}
}
\value{
A list with a data table and a plot for the selected week.
}
\description{
Generate summary for heart rate for a week.
}
