# Runlogs
This is a shiny application to log runs.
It writes and reads from an SQLite database and attempts to provide useful data.
While it has reached a state in which it can be used, there is still active
development, so there might be some rough edges here and there.

The application pretty much assumes that it is run on Unix; as I don’t have 
a machine to test it on right now I do not know whether it works on Windows. 

## Installation
You can install the latest version with 
`devtools::install_gitlab("choh/runlogs")`.

When installing the application you will have to launch it once manually from 
within R by calling `runlogs::launch_app()`.
It should then place a script in `.local/bin` you can use to launch it 
afterwards.

As the application does not create the parent directories yet, you might
need to create the `.local/bin` or `.local/share` locations manually first for
the installation to succeed. This will be fixed in a future version.