copy_db <- function(db, file_name = NULL) {
#' Create a backup of the existing database.
#'
#' @param db The database file.
#' @param file_name Optional: the name to use for the copy.
#'
#' @return Invisibly whether the operation succeeded.
#'
#' @keywords internal
#' @export
    if (is.null(file_name)) {
        file_name <- paste0(db, ".bak")
    }

    res <- file.copy(db, file_name)
    invisible(res)
}
