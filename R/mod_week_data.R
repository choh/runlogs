mod_week_data_ui <- function(id) {
#' Create the plot summarising a week.
#'
#' @param id The id for the UI module.
#'
#' @return The UI elements for the week visualisation.
#' @export
    ns <- NS(id)
    column(10,
        plotOutput(ns("distance_plot")),
        DT::dataTableOutput(ns("summary_tbl"))
    )
}

mod_week_data <- function(input, output, session, conf, weekstr, stat) {
#' Server part for the week visualisation.
#'
#' @param input Shiny input.
#' @param output Shiny output.
#' @param session Shiny session.
#' @param conf Runlogs shiny app config.
#' @param weekstr The week for the visualisation in the "week-year" format.
#' @param stat Either dist or hr.
#'
#' This will populate the UI elements for the visualisation.
#'
#' @export
    ns <- session$ns
    date_split <- unlist(strsplit(isolate(weekstr()), "-"))
    selected_stat <- isolate(stat())

    year_extract <- date_split[1]
    week_extract <- date_split[2]

    week_data <- week_distance_summary(
        conf$db, week_extract, year_extract)

    # FIXME: make this cleaner
    if (selected_stat == "hr") {
        week_data$plot <- week_hr_summary(
            conf$db, week_extract, year_extract)$plot
    }

    output$distance_plot <- renderPlot({ week_data$plot })

    tbl_data <- week_run_tbl(conf$db, week_extract, year_extract)

    if (nrow(tbl_data) > 0) {
        detail_buttons <- lapply(seq_along(1:nrow(tbl_data)),
            function(x) as.character(
                a(
                    id = ns(paste("details", tbl_data[[x, "ID"]], sep = "_")),
                    href = "#",
                    onclick = paste0("Shiny.setInputValue('",
                                     "detail_press", "', this.id, ",
                                     "{priority: 'event'});"),
                    "Details"
                )
            ))
        detail_buttons <- unlist(detail_buttons)
        tbl_data <- cbind(tbl_data, detail_buttons)
        colnames(tbl_data)[ncol(tbl_data)] <- " "
        no_escape <- -1 * ncol(tbl_data)
    } else {
        no_escape <- TRUE
    }

    output$summary_tbl <- DT::renderDataTable(
        tbl_data[, -1], escape = no_escape, selection = "none",
        options = list(lengthChange = FALSE, searching = FALSE,
                       paging = FALSE))
}
