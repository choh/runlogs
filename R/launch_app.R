launch_app <- function(dev = FALSE) {
#' Launch the shiny app to interact with the database.
#' @param dev It true try launching from inst/shiny, if false from the
#' package directory.
#' @export
    if (dev) {
        app_path <- file.path("inst", "shiny")
        browser <- FALSE
    } else {
        app_path <- system.file("shiny", package = "runlogs")
        browser <- TRUE
    }
    shiny::runApp(app_path, launch.browser = browser)

}
