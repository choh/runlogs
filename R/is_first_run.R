is_first_run <- function() {
#' Check if this is the first time the application runs.
#'
#' Look whether config files and database do exist and create if necessary.
#' @keywords internal
    default_config_dir <- "~/.config/runlogs"
    default_config_file <- "~/.config/runlogs/runlogs.yml"
    default_data_dir <- "~/.local/share/runlogs"
    default_bin <- "~/.local/bin/runlogs"
    default_css <- "~/.local/share/runlogs/runlogs.css"
    default_last_run <- "~/.local/share/runlogs/track_ver"
    default_route_dir <- "~/.local/share/runlogs/routes"
    db_path <- file.path(default_data_dir, "runlogs.db")

    if (!dir.exists(default_config_dir)) {
        dir.create(default_config_dir, mode = "755")
        file.copy(system.file("defaults/runlogs.yml", package = "runlogs"),
                  default_config_file)
    }

    current_version <- gsub("'", "", utils::packageVersion("runlogs"))



    if (!dir.exists(default_data_dir)) {
        dir.create(default_data_dir, mode = "755")
        con <- DBI::dbConnect(RSQLite::SQLite(), db_path)
        DBI::dbBegin(con)
        sql <- readr::read_file(
            system.file("sql/running.db.sql", package = "runlogs"))
        sql_split <- strsplit(sql, ";")
        sql_split <- unlist(sql_split)
        sql_split <- sql_split[-length(sql_split)]
        r <- lapply(sql_split, function(x) DBI::dbExecute(con, x))
        DBI::dbCommit(con)
        DBI::dbDisconnect(con)
    }


    if (!file.exists(default_last_run)) {
        last_run_version <- "0"
    } else {
        fh <- file(default_last_run, "r")
        last_run_version <- readLines(fh)
        close(fh)
    }

    if (last_run_version != current_version) {
        overwrite_bin <- TRUE
        fh <- file(default_last_run, "w")
        writeLines(current_version, fh)
        close(fh)
    } else {
        overwrite_bin <- FALSE
    }


    if (!file.exists(default_bin) | overwrite_bin) {
        file.copy(system.file("defaults/runlogs", package = "runlogs"),
                  default_bin)
    }

    # add CSS for version 0.2
    if (!file.exists(default_css)) {
        file.copy(system.file("defaults/runlogs.css", package = "runlogs"),
                  default_css)
    }

    # and also make sure the style is included in the config
    conf <- runlogs::read_config(default_config_file)
    if (!"style" %in% names(conf)) {
        cat(paste0("    style: \"", default_css, "\"\n"),
            file = default_config_file, append = TRUE)
    }

    # add routes dir for version 0.3 and move files to that directory
    if (!dir.exists(default_route_dir)) {
        dir.create(default_route_dir, mode = "755")
        migrate_routes_to_centralised_location(db_path)
    }

    if (last_run_version != current_version) {
        file.copy(system.file("defaults/runlogs.css", package = "runlogs"),
                  default_css, overwrite = TRUE)
    }
}
