CREATE TABLE IF NOT EXISTS `runs` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`run_date`	TEXT,
	`week`	NUMERIC,
	`duration`	REAL,
	`distance`	REAL,
	`pace`	REAL,
	`hr_avg`	REAL,
	`hr_max`	REAL,
	`route_id`	INTEGER,
	FOREIGN KEY(`route_id`) REFERENCES `route`(`id`)
);
CREATE TABLE IF NOT EXISTS `route` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`file`	TEXT,
	`name`	TEXT,
	`distance`	REAL
);
CREATE TABLE IF NOT EXISTS `comment` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`comment`	TEXT,
	`run_id`	INTEGER,
	FOREIGN KEY(`run_id`) REFERENCES `runs`(`id`)
);
