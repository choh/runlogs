# Changelog

## 0.3.1, 2021-01-03

* Fix handling of ISO weeks 53 and 1.


## 0.3, 2020-06-02

* Add detail view for runs, you can now see the comments you have entered
* Add the option to delete runs
* Add statistics tab displaying more summaries on distance
* Weeks now start on Monday in date picker (in the future this should be an option)
* Move routes to a location in .local/share and make filenames unique
* When adding routes display the length of the route before saving
* On starting the application a backup of the database is made
* Start adding automated tests to improve code quality
* Modularise large parts of the application for cleaner code


## 0.2, 2020-03-01

* Add CSS, so the UI looks cleaner
* Add overview tables
* Add HR summaries
* Values update when adding runs/routes


## 0.1.1, 2020-02-03

* Remove unncessary `library` call
* Fix missing explicit namespaces


## 0.1.0, 2020-02-02
Initial version, allows for saving runs and routes, visualising routes and 
gives some distance summaries. 
